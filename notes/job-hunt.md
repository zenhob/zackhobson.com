---
tags: draft
---

# What am I looking for?

- A business that isn't funded by VC
- A business where I can relate to or empathize with the customer
- A team that is small enough to make an individual impact

# Accomplishments

## Insights

- solo developed lookup tables, including the user-facing UI and the NRQL parser
- interviewed countless developers by pairing on real code, built a successful team
- mentored and paired on new features with interns, like Iris and the markdown widget
- "A year and a half of roadmap in 12 months." -Henry Shapiro
- led team discussions about software health across the company
- built a suite of custom UI components for other teams to use

## Workfrom

- built a high-quality user-facing UI by hand
- kept a daily developer log for colleagues, recording and justifying technical decision points
- set up fully automated builds using gitlab and docker
- audited and fixed SQL injection issues in a legacy codebase
- developed team working agreement:

```
    Review completed work.
    Congratulate each other on a job well done.
    Produce a report on what we got done!
    Clear out completed work from the "Done" column.
    Review incomplete work.
        this may roll over into the current sprint but it doesn't have to
    Create tickets for upcoming work if needed.
        use the upcoming work column as a possible source of tickets
        use the Product Sprints board to make sure that we're tracking our roadmap
    Determine our sprint goals based on what we think we can ship.
        this is if everything else goes well and there are no surprises
        consider the time that your other commitments take up (including product and sprint planning)
    Determine if there are good stretch goal candidates for the sprint.
        this is often work that we'd normally start on in the next sprint
    Assign all of these tickets to the sprint, and mark stretch goal tickets with a tag so we can identify them.
```
