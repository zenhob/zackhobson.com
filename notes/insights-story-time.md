Just trying to write down what I remember from my time at New Relic.

# Matterhorn

Some time after I joined the Insights team, a product manager at New Relic
decided to pivot to a new product for user analytics, code-named Matterhorn. I
wasn't happy about this news, mostly because I'd joined New Relic to work on
tools for developers. Matterhorn targeted suits who wanted to know user metrics
in real time. While the underlying technology (dashboard charts of massive
custom event queries) was the same, the customer was very different. Despite
this, the team pivoted dutifully and developed the new app, queries, and
visualizations needed for this product. Of course, this meant that work on
Insights was halted.

I guess Matterhorn didn't take off because it was scrapped after a while.
Management pulled the team into a meeting and asked us how we felt about
sticking with Matterhorn or resuming work on Insights.

# Upscale

Upscale was an event hosted by consultant Jim Shore that gave pretty much every
contributor in the company the chance to choose their team. At this time, the
Insights team had been through the wringer with Matterhorn, and many of our
early team members were feeling burnt out. This means that when the big shuffle
happened, the two remaining developers from the original team jumped ship.

After this, a representative of each team got up and pitched to the gathered
developers. Our team manager (the second one we had, our first manager had
already been promoted away from us) got up and pitched. I don't remember
exactly what he said, but I remember thinking that it was distinctly
uninspiring. It drove me nuts thinking that we might miss our chance to pick up
folks who'd want to work on this product! The moment he stepped off the stage,
I asked him to please, PLEASE let me pitch next time.

I had my chance soon after. When I got up in front of the group, I don't
remember exactly what I said. I was powered by pure enthusiasm for our product
and mission. We had the coolest product at the company! We were solving
problems nobody was getting to solve, for some of the biggest customers we had!
It was a chance to work with me, I'm so much fun! Whatever I said, it did the
trick and we picked up several teammates and ended up with a full team.

# Mobbing

* Bryant suggested it
* Henry was horrified at it
* Both of them think that my buy-in may have been what did it
* Ended up being one of the most productive teams at the company, tore through two years of planned features in six months
* When someone was out for the day (even our Java expert!) things didn't slow down
* Interns and new teammates got to shine and learned fast

