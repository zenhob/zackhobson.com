Installed nix package manager to try it out, and I'm using it to  set up a dev env.

# Helix

Helix is a text editor that functions a bit like Vim, and it seems to have more of
the modern niceties built in. My vim config is really getting too big, if I can get
language server support and fuzzy match built in I am going to take it.

In order to do basic things in helix I needed to pull in some vim-alike bindings
to get around without constantly being disoriented. I think I will be able to
remove them one at a time as I learn to use helix: https://github.com/LGUG2Z/helix-vim

Helix has a command to edit the config file: `:config-open`, pretty useful.


