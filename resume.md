---
title: 'Zack Hobson'
layout: layouts/resume.html
---

# Zack Hobson

> zack@zackhobson.com
>
> [linkedin.com/in/zackhobson](https://www.linkedin.com/in/zackhobson)

## Experienced Full-Stack Product Engineering and Process Leadership

Specialist in creating customer value through iteration. Built and shipped a
complete SaaS product for remote meetings and focus sessions. Enabled
enterprise data querying and visualization for Fortune 500 companies. Shipped
support and development tools used by popular open source projects.

## Skills Overview

- Ruby, Javascript, Typescript, Go, Python, CSS, HTML
- Postgres, Docker, Linux, AWS, DigitalOcean
- DevOps, process management, remote collaboration

## Recent Professional Experience

### Co-founder + Head of Product Engineering

**[Workfrom](https://www.workfrom.com/)** / Portland, OR / 2020-2023

### Senior Software Engineer

**[New Relic](https://newrelic.com/)** / Portland, OR / 2014-2019

### Senior Consultant

**[Cloud City Development](https://cloudcity.io/)** / San Francisco, CA / 2013

### Senior Software Engineer

**[ENTP](http://entp.com/)** / Portland, OR / 2009-2012

### Rails Technology Lead

**[OpenSourcery](http://www.opensourcery.com/)** / Portland, OR / 2008-2009

### Previously

- Senior Software Engineer, Geni / Los Angeles, CA
- Senior Software Engineer, Shopzilla / Los Angeles, CA
- Software Engineer, Namesafe / Los Angeles, CA
- Software Engineer, Cnation / Los Angeles, CA
- Desktop Support, Richmond-Monroe Group / Santa Clarita, CA
- System Administrator, Smartlink / Valencia, CA
