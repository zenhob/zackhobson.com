---
title: 'Zack Hobson'
layout: layouts/resume.html
---

# Zack Hobson

> zack@zackhobson.com
>
> [linkedin.com/in/zackhobson](https://www.linkedin.com/in/zackhobson)

## Experienced Product Engineering and Process Leadership

My specialty is the design and implementation of web-based products, with a
focus on collaboration and creating customer value through iteration.

I believe large-scale software development is fundamentally a social endeavor.
Collaboration is at the core of what we do and is essential to scaling projects
as well as teams.

## Skills Overview

- Most recently, spent 2 years planning, implementing and shipping [SaaS for
  remote meetings and focus sessions](https://www.cafeforteams.com/)
- Built numerous applications using many different technologies, including Ruby
  on Rails, Node, Postgres, Docker, Typescript, React and more
- Expert in collaborative development practices, including project planning and
  mob programming
- Product-oriented approach, laser-focused on customer value

## Professional Experience

### Co-founder + Head of Product Engineering

**[Workfrom](https://www.workfrom.com/)** / Portland, OR / 2020-2023

- Helped Workfrom get from an idea to a SaaS product with paying customers in
  less than 2 years.
- Guided the development process of our main product and shipped value on a
  fast consistent cadence, with weekly production demos.
- Built and maintained deployment resources for production and development
  environments.
- Maintained a development blog for product and business stakeholders that
  provided research results, information about product developments, and
  detailed breakdowns of technical issues facing the product organization.
- Supervisor: Darren Buckner

### Senior Software Engineer

**[New Relic](https://newrelic.com/)** / Portland, OR / 2014-2019

- Joined and eventually led a team of engineers to maintain, support and add
  features to New Relic Insights, a powerful analytics platform for querying,
  visualizing and dashboarding custom event data
- Worked very closely with both design and product management to ensure the
  engineering team was closely matching the product vision
- Mentored early-career engineers, helping them strategize about their career
  and develop their own projects
- Facilitated software health evaluations in an engineering-wide effort,
  leading discussions with entire teams about the overall health of their
  software projects
- Helped to implement and run emergency response drills, which involved
  deploying our entire infrastructure to another datacenter in event of a
  disaster
- Supervisor: Jon Karon

### Senior Consultant

**[Cloud City Development](https://cloudcity.io/)** / San Francisco, CA / 2013

- Built [Deathstare][] to stress-test a mobile app back-end before it went live
  to a massive audience
- Used Heroku and Librato to deploy and report on up to 20,000 concurrently
  running tests
- Assisted junior developers with design and performance issues in the main
  product
- Supervisor: Kenzi Connor

[Deathstare]: https://github.com/cloudcity/deathstare

### Senior Software Engineer

**[ENTP](http://entp.com/)** / Portland, OR / 2009-2012

- Fixed bugs and added features to Lighthouse and Tender, both large and
  widely-used legacy Rails applications
- Interacted directly with Lighthouse and Tender customers to diagnose and
  solve their problems
- Assisted in moving dashboard indexing and lookup to Redis, greatly improving
  performance
- Implemented the back-end for the redesign of Tender's administrative UI
- Planned and supported Tender's transition to a new payment system
- Implemented Tender's front-end theme system using Knockout and Less
- Supervisor: Courtenay Gasking

### Rails Technology Lead

**[OpenSourcery](http://www.opensourcery.com/)** / Portland, OR / 2008-2009

- Responsible for guiding Ruby on Rails development processes and selecting
  tools and components
- As lead maintainer of the open-source EHR software testing system Laika,
  helped to turn it from a prototype into a working product
- Implemented two-way SOAP-based interaction with an existing software system
  to facilitate a public web-based front end
- Supervisor: Brian Jamison

### Senior Software Engineer

**[Geni](https://www.geni.com/)** / West Hollywood, CA / 2007-2008

- Assisted in the maintenance of a large and widely-used application written in
  Ruby on Rails
- Designed and implemented numerous features including individual and aggregate
  timelines, events, messaging, family discussion and last name index
- Took the lead in improving testing methodology, including replacing fixtures
  with factories and employing true unit and functional tests
- Supervisor: Amos Elliston

### Senior Software Engineer

**[Shopzilla](http://www.shopzilla.com)** / Los Angeles, CA / 2005-2007

- Assisted in the maintenance of an enormous legacy code base in Perl
- Assisted in the redesign and implementation of the URL/link subsystem to
  support internationalization and SEO-optimization
- Designed and implemented pure-Perl DSLs for configuring virtual hosts and
  dispatch for dozens of web properties
- Designed features and supervised their implementation by junior developers
- Supervisor: Adam Pisoni

### Previously

- Software Engineer, Namesafe / Los Angeles, CA
- Software Engineer, Cnation / Los Angeles, CA
- Desktop Support, Richmond-Monroe Group / Santa Clarita, CA
- System Administrator, Smartlink / Valencia, CA
