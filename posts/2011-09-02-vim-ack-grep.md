---
title: You don't need a plug-in to use ack with Vim
---

[ack] is well known as one of the best command-line search tools, and there are
plug-ins for many popular editors, including for some reason Vim. I've not been
able to determine why these exist for Vim, as support is already built-in:

    :set grepprg=ack\ -aH

See, Vim already has built-in support for external grep, so by telling ack to
output the same format we can use it as a drop-in replacement:

    :grep foo dir1 dir2

This populates the quickfix window, so you can use `copen` or `cnext` to
list them or skip to the first one, respectively. In fact all the
grep-related commands  will work as usual, except that ack is doing all the
work underneath.

[ack]: http://betterthangrep.com/
