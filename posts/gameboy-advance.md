---
title: There is a modern Game Boy Advance development toolchain
summary: In which I finally find a use for some old hardware
tags: draft
eleventyExcludeFromCollections: true
---

The Game Boy Advance is one of my favorite video game consoles. I still have my
[GBA micro][] and a pile of games. Metroid Zero Mission and Mega Man Battle
Network 2 still hold up pretty well! I also have a flash cartridge that I
wanted to use to make my own GBA software, but the tools were not very
accessible at the time (this was about 15 years ago).

This is changing! In fact it turns out there is a pretty active [GBA homebrew
community][] and a variety of [GBA software development tools][]. Add in a good
emulator (I'm starting with [mGBA][]) and you have everything you need for
homebrew console development.

My intent is to record this process so that I can look it up later. 😅
