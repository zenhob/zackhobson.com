---
title: "Detached for Mac"
---

*A version of this post originally appeared on the [Cloud City blog][blog].*

Many developers are accustomed to working in a terminal window.
Even on the Mac, with a high resolution display and accelerated graphics, much
of the tools we use every day are operated through a terminal emulator. I
personally worked this way before I used a Mac and I still do today. In fact,
every Mac comes with a great tool for managing multiple terminal sessions: [GNU
Screen][]. Unfortunately, it can be hard to use, and it provides no visibility
of your running sessions from the GUI.

[Detached][] is a tool for your Mac that sits in your menu bar and lets you create
terminal sessions that you can close and reopen later, without losing the associated
process or your place in it (until you reboot).
This is similar to how sessions work in most document-based applications:
If you close the application with work in progress, you expect it to restore that
state when you reopen it. Thanks to GNU Screen, this functionality is already
built into your Mac: Detached just exposes it by making your terminal sessions
accessible from your menu bar.

![screenshot][]

The [first version of Detached][detached-orig] was written about 3 years ago, using [MacRuby][].
At the time, it was difficult to distribute MacRuby binaries, because they had to
include the entire MacRuby runtime in order to be portable. The first distributable
binary for the original version of Detached was over 70 megabytes!

It only took a few days to create the initial version of Detached, and
once I'd gotten it chugging along it didn't need much work. It had been a fun
experience, but I wasn't ready to build MacRuby apps for a living or anything.
However, I still had Detached, and I ended up using it nearly every day for the
next three years! By that time, I'd gone independent, there had been umpteen
releases of Xcode, and the original Detached project was getting pretty hard to 
wrangle. I decided to rewrite it in Objective-C.

Rewriting was easy! Cocoa and Xcode have come a long way in the last three years
and I had no trouble getting up to speed on ARC memory management and the like.
The resulting binary was less than one megabyte, and two orders of magnitude
smaller than the original. That's much easier to distribute!

The only thing I was missing was an icon for the application. I wasn't really
keen on trying to do this myself, so I brought it to [Stephanie Geerlings][]
and [Brendan Miller][] of [Cloud City Development][], who
worked with me through several drafts before we settled on this fellow:

![logo][]

I love this logo because it's simple, memorable and charming.
Earlier revisions tried to maintain some metaphor or theme relating to the
software itself, but in the end it was more important to me that the image be
appealing and fun. I think Brendan did a fantastic job.

My original plan was to distribute on the App Store,
but I'd need to sandbox the app, which would mean not using AppleScript to power
Terminal and iTerm, which is the core functionality! Instead, I am
[distributing Detached myself][detached].
The [source code for Detached][detached-source]
is also available under the terms of the [GPL][].

[blog]: http://blog.cloudcitydevelopment.com/2013/12/19/detached-for-mac/
[logo]: /images/detached512.png
[screenshot]: /images/detached-screenshot-1.png
[GNU Screen]: https://www.gnu.org/software/screen/
[Detached]: http://zackhobson.com/detached/
[detached-orig]: http://github.com/zenhob/detached/
[MacRuby]: http://macruby.org/
[Stephanie Geerlings]: http://twitter.com/geerlinger/
[Brendan Miller]: http://digiguin.com/
[Cloud City Development]: http://cloudcity.io/
[detached-source]: http://github.com/zenhob/DetachedObjc/
[GPL]: https://gnu.org/licenses/gpl.html
