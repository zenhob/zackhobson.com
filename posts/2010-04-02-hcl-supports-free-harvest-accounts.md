---
title: HCl 0.3.0 supports free Harvest accounts!
---

This morning I received two pull requests for the same issue in [HCl][], my
command-line client for the [Harvest][] time-tracking service:
 
![Pull requests!](http://zackhobson.com/images/hcl-pull-requests.jpg)

They were both fixes for a [long-running problem][issue28] with free
Harvest accounts that I just never got around to addressing. It surprised me
to see this activity after so long, but when I checked my fork queue I found
a number of commits for which I'd never received pull requests. What a nice
surprise!

Anyway, thanks in part to the efforts of [Michael Bleigh][mbleigh] the latest
version of [HCl][] supports non-SSL (read: free) Harvest accounts. Yay!

[HCl]: http://zackhobson.com/hcl
[Harvest]: http://www.getharvest.com/
[issue28]: http://github.com/zenhob/hcl/issues/issue/28
[mbleigh]: http://github.com/mbleigh
