---
title: New features and fixes in HCl 0.4.x
summary: "Recent releases of HCl include improvements to aliases, multiple accounts and more."
---

[HCl][] has always just been about scratching an itch. I'd built it years ago when
I needed to track my hours, and stopped maintaining it when I got a gig that
didn't. In the late summer I started using [Harvest][] again on a
client project, and the experience made me realize that HCl still
needed some work. The result was several weeks of bug-fixing and enhancements.

### Improvements to task aliases

When I went back to using HCl, the biggest pain point was specifying and using
task aliases. I had to look up the `set` command every time, and starting a timer
felt cumbersome:

    hcl set task.mytask 123 456
    hcl start mytask some stuff

To address this, I made some changes to the start command, to make the command
name optional and allow the task name to be specified anywhere with `@`. I also
added a simpler `alias` command for aliasing tasks:

    hcl alias mytask 123 456
    hcl @mytask some stuff

### Multiple Harvest accounts

You can use an alternate configuration by specifying `HCL_DIR` in the environment.
This allows you to switch between multiple accounts quickly. For example, adding
something like this to your shell configuration will create a `myhcl` command with
separate account credentials:

    alias myhcl="env HCL_DIR=~/.myhcl hcl"

You can then use `myhcl` and `hcl` with different account credentials to easily
switch accounts.

### Cancel a running timer

You can cancel a running timer (or the most recently stopped timer) with
`hcl cancel`. You can also use `hcl nvm` and `hcl oops`.

### Auto-completion of task aliases

You can enable completion of task aliases in Bash by adding this to your bashrc:

    eval `hcl completion`

### Refresh or review your Harvest credentials

You can now refresh your auth credentials with the `--reauth` option, and
review them with `hcl config`. HCl also refreshes your credentials automatically
on auth failure.

### Log entries without a timer

At user request, there is a new command `hcl log` that you can use to log
an entry without leaving a timer running:

    hcl log @mytask +1.25 Finally did that thing.

### MacOS X only: Passwords are now stored in your keychain

On MacOS X your password is now stored in your keychain!
This behavior is automatic, you don't need to enable it.
Unfortunately it's not available on any other platform yet.

### More improvements and fixes since 0.3.x

* Added support (and continuous integration) for several modern Ruby platforms.
* The `note` command with no arguments displays all notes for a running timer.
* Allow filtering of `tasks` by project code.
* The `stop` command now checks for running timers from the previous day.
* The `resume` command now accepts an optional task.
* Added support for auto-retry on API throttle.

[HCl]: http://zenhob.github.io/hcl
[Harvest]: http://www.getharvest.com/

