---
title: Easier Sessions with MacVim
---

***Given:*** You never want to close your Vim session because you lose your lovingly crafted
(haha!) buffer list, and in my case, open tabs. I am not here to convince you that you
want to use sessions with Vim, that's up to you. Suffice to say that if you
don't use editor sessions this post will not be of much use to you.

### The Problem

I was thinking it'd be nice if it was a little easier to open Vim sessions on
my Mac.  Usually I use [Alfred](http://www.alfredapp.com/) to start
[MacVim](http://code.google.com/p/macvim/) then "ESC-:session
~/Projects/blablah/Session.vim", tab-completing as appropriate. I can't just
bind a key to this command because I have to specify the session file. If I am
in a Terminal the process is pretty direct (I can use mvim -S filename) but
usually I need an editor session before I need a Terminal.

Alternately, I can type "open session.vim" in Alfred to open the session file in a MacVim
buffer and then "ESC-:so %", which maybe is technically easier, but I figured at that
point I'm not far from being able to just open MacVim with that session automatically,
right?

So I googled around a bit and found this [useful article on the subject](http://vim.wikia.com/wiki/Vim-sessions_under_Mac), which was almost exactly what I was looking for, with some caveats:

1. It requires that you save your session files with an extension besides ".vim" (I used
   the suggested ".vis").
2. The app kept popping up the "You haven't run this application before" dialog on every
   launch.
3. My bash environment is never loaded by the Automator app, so my path and such are not
   set appropriately. This messes with my Vim plugins that use external programs.

### The Solution

Originally this post was about how to solve the problems listed above, which is possible
to do in all three cases. However, I began to realize that there was probably a simpler way
to accomplish this. This realization hinged on the fact that I didn't really ever want to
edit my Vim session files, that this was what differentiated them from other Vim scripts.

So I simplified the process to the following steps (note that Automator is no longer
involved):

1. Set up the Mac OS file associations such that .vis files are opened automatically with
   MacVim.
2. Add the following to your $HOME/.gvimrc:

<pre>
" save sessions with .vis extension
map &lt;leader&gt;s :mksession!  session.vis&lt;CR&gt;

" automatically source vim sessions so I can open them with the finder
au BufRead *.vis so %
</pre>

I decided to put this in my gvimrc since I don't really use sessions in console Vim
(that's more for one-off stuff than project work), and it might be useful just in case I
do need to edit a session file for some reason. Also I elected to stick to explicit
session saving rather than autosave. Personally I don't mind saving sessions manually as I
don't want to do it every time and I'm used to saving my files anyway.

So now, Alfred's open file functionality lets me instantly dial up any of my
MacVim sessions:

<img src="http://zackhobson.com/images/alfred-macvim-sessions.jpg" alt="Illustration: Alfred search box">

When I select one of these it opens a MacVim window with the session loaded. A
drastic reduction in keystrokes, and no need to involve Automator. Using a
different file extension means that I can more easily select from all of my
available sessions.
