const pluginRss = require('@11ty/eleventy-plugin-rss')

module.exports = function (eleventyConfig) {
  eleventyConfig.setTemplateFormats([
    // Templates:
    'html',
    'liquid',
    'njk',
    'md',
    'xml',
    // Static Assets:
    'txt',
    'css',
    'jpeg',
    'jpg',
    'png',
    'svg',
    'woff',
    'woff2',
  ])
  eleventyConfig.addPassthroughCopy('static')
  eleventyConfig.addPassthroughCopy('images')
  eleventyConfig.addPlugin(pluginRss)

  return {
    dir: {
      input: '.',
      includes: '_includes',
      output: 'public',
    },
    markdownTemplateEngine: 'liquid',
  }
}
