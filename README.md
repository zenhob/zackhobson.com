---
title: About
layout: layouts/main.html
permalink: '/about/'
---

## About ZackHobson.com

Static blog powered by [Eleventy](https://11ty.dev). Source documents utilize
the [Liquid template language](https://shopify.github.io/liquid/).

All public-facing documents are written in Markdown with HTML layouts. Blog
posts are in `posts/`, configured with `posts/posts.json`. Random notes are in
`notes/`, configured with `notes/notes.json`.

### Important Files

#### `package.json`

Specifies scripts and module dependencies.

#### `.eleventy.js`

Configures supported template formats and source/target file paths.

#### `tsconfig.json`

Configures TypeScript compiler to support JavaScript type checking.

#### `.s3-website.json`

Configure the s-website tool for deploying the site to S3. Uses
`AWS_SECRET_ACCESS_KEY` and `AWS_ACCESS_KEY_ID` from the environment or the
`.env` file, if it exists.

### Available Scripts

#### `yarn start`

Runs the app in the development mode. Open `http://localhost:8433` to view it
in the browser. The page will hot-reload when source files change.

#### `yarn deploy`

Do a static check of the JavaScript code, build the website, and deploy to S3.

### Other commands

#### `tsc`

Use the TypeScript compiler to statically verify the JavaScript code.
Currently the code is not transformed or compiled, just analyzed.
