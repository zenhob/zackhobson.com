---
title: 'Zack Hobson'
layout: layouts/posts.html
---

{% assign posts = collections.post | reverse %}
{% for post in posts limit:7 %}
  <h2>
    <a href="{{ post.url }}">{{ post.data.title }}</a>
    <span class="alt">{{ post.date | date: "%A %B %e, %Y" }}</span>
  </h2>
{% endfor %}
