'use strict'

class NotesSearch extends HTMLElement {
  static Style = `
    :host {
      --form-border: #ccc;
      --form-bg: #fefefe;
      --focus-border: blue;
      --input-text: #000;
      --alt-item-bg: #eee;
    }
    * {
      box-sizing: border-box;
    }
    form {
      display: inline-flex;
      outline: 1px solid var(--form-border);
      border-radius: 0.25rem;
      padding: 0.5rem;
      width: 100%;
      background: var(--form-bg);
    }
    form:focus-within {
      outline: 1px solid var(--focus-border);
    }
    input:focus {
      outline: none;
    }
    input {
      color: var(--input-text);
      background: transparent;
      flex: 1 0;
      padding: 0.5rem;
      border: 0;
      font-size: 14px;
      font-weight: 500;
    }
    button {
      display: none;
    }
    ul {
      margin-top: 0;
      padding-left: 0;
      font-size: 14px;
      background: transparent;
      padding: 1ex;
      border-radius: 0.25rem;
    }
    ul li {
      padding: 0.25rem 1rem;
      background: var(--form-bg);
      list-style: none;
    }
    ul li:nth-of-type(2n) {
      background-color: var(--alt-item-bg);
      border-radius: 0.25rem;
    }
  `

  constructor() {
    super()

    const wrapper = document.createElement('div')
    const form = wrapper.appendChild(document.createElement('form'))
    const input = form.appendChild(document.createElement('input'))
    input.setAttribute('name', 'q')
    input.setAttribute('autocomplete', 'off')
    input.setAttribute('placeholder', this.getAttribute('data-placeholder'))
    const button = form.appendChild(document.createElement('button'))
    button.textContent = 'Search'
    const results = wrapper.appendChild(document.createElement('ul'))

    form.addEventListener(
      'submit',
      this._searchSubmitFormHandler(input, results),
    )
    input.addEventListener('focus', () => input.select())

    // we'll need this in the connectedCallback
    this.input = input
    this.wrapper = wrapper

    // cache the notes index data
    this._fetchNotesIndex()
      .then(index => {
        this.index = index
      })
      .catch(e => {
        alert(`Failed to fetch notes index: ${e}`)
      })
  }

  connectedCallback() {
    this.attachShadow({ mode: 'open' })
    const style = document.createElement('style')
    style.textContent = NotesSearch.Style
    this.shadowRoot.append(style, this.wrapper)
    this.input.focus()
  }

  // fetch the notes index
  async _fetchNotesIndex() {
    const notesUrl = this.getAttribute('data-src')
    try {
      const request = await fetch(notesUrl)
      return await request.json()
    } catch (e) {
      console.error('Failed to fetch notes index.', e)
      return {}
    }
  }

  /**
   * @param {HTMLInputElement} input
   * @param {HTMLUListElement} results
   * @return {(e: Event) => Promise<void>}
   */
  _searchSubmitFormHandler(input, results) {
    return async e => {
      e.preventDefault()
      if (!input.value) {
        results.replaceChildren()
        return
      }
      const matchValue = `${input.value}`.toLowerCase()

      /**
       * @typedef {object} Note
       * @property {string} title
       * @property {string} url
       */

      const resultRows = this.index.notes
        .filter(
          /** @param {Note} note */ note =>
            note.title.toLowerCase().indexOf(matchValue) >= 0,
        )
        .map(
          /** @param {Note} note */ note => {
            const row = document.createElement('li')
            const item = row.appendChild(document.createElement('a'))
            item.textContent = note.title
            item.href = note.url
            return row
          },
        )
      results.replaceChildren(...resultRows)
      input.select()
    }
  }
}

customElements.define('notes-search', NotesSearch)
